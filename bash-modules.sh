# Maintainer: Caleb Cooper
#
# This BASH script is intended to include functions which can be
# called after the .gitlab-ci-before-script.yml file has been included.

func_parallel() {
  declare -i PID=$$

  declare ACTION
  declare INPUT
  declare -i NUM_PROCESSES
  while getopts "a:i:n:" OPTION; do
    case ${OPTION} in
      a)
        ACTION="${OPTARG}";;
      i)
        INPUTS="${OPTARG}";;
      n)
        NUM_PROCESSES=${OPTARG};;
    esac
  done
  if [[ -z "${ACTION}" ]] || [[ -z "${INPUTS}" ]] || [[ -z "${NUM_PROCESSES}" ]]; then
    printf "All three flags are required:\n\t-a) The action to be executed by each process.\n\t-i) The input list from which new processes will pull data.\n\t-n) Number of parallel processes.\n"
    exit 1
  fi
  
  declare -a INPUTS_ARRAY=( $(eval "${INPUTS}") )
  for INPUT in ${INPUTS_ARRAY[*]}; do 
    mapfile -t CHILDREN < <(pgrep -P $PID)
    while [[ ${#CHILDREN[@]} -gt ${NUM_PROCESSES} ]]; do
      read -r -s -t 0.1
      mapfile -t CHILDREN < <(pgrep -P $PID)
    done
    ${ACTION} ${INPUT} &
  done
  wait
}

func_count_down() {
  declare -i MIN="${1}"
  declare -i MAX="${2}"
  declare -i INT="${3}"

  for (( I = ${MAX} ; I >= ${MIN} ; I -= ${INT} )); do
    printf "%i\n" ${I}
    sleep ${INT}
  done
}

func_partial_clone() {
  declare INPUT_GIT_PATH="${1}"
  declare INPUT_GIT_URL="${2}"
  shift 2
  declare -a INPUT_GIT_PARTS=( ${*} )

  mkdir "${INPUT_GIT_PATH}"
  git --work-tree "${INPUT_GIT_PATH}" --git-dir "${INPUT_GIT_PATH}/.git" init
  git --work-tree "${INPUT_GIT_PATH}" --git-dir "${INPUT_GIT_PATH}/.git" remote add -f origin "${INPUT_GIT_URL}"
  git --work-tree "${INPUT_GIT_PATH}" --git-dir "${INPUT_GIT_PATH}/.git" config core.sparseCheckout true
  for INPUT_GIT_PART in ${INPUT_GIT_PARTS[*]}; do
    echo "${INPUT_GIT_PART}" >> "${INPUT_GIT_PATH}/.git/info/sparse-checkout"
  done
  git --work-tree "${INPUT_GIT_PATH}" --git-dir "${INPUT_GIT_PATH}/.git" pull --quiet --no-tags --depth=1 origin master
}
